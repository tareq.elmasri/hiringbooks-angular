(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('Requests', Requests);

  /** @ngInject */
  function Requests(Restangular) {
    var service = Restangular.all("requests");
    service.addRestangularMethod('delete_request', 'post', 'delete_request');
    service.addRestangularMethod('status_book', 'post', 'status_book');
    service.addRestangularMethod('comment_book', 'post', 'comment_book');
    return service;
  }

})();
