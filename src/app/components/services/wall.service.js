(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('Wall', Wall);

  /** @ngInject */
  function Wall(Restangular) {
    var service = Restangular.all("wall");
    service.addRestangularMethod('set_seen', 'post', 'set_seen');
    service.addRestangularMethod('delete_post', 'post', 'delete_post');
    service.addRestangularMethod('comment_post', 'post', 'comment_post');
    return service;
  }

})();