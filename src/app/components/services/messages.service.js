(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('Messages', Messages);

  /** @ngInject */
  function Messages(Restangular) {
    var service = Restangular.all("messages");
    service.addRestangularMethod('set_seen', 'post', 'set_seen');
    return service;
  }

})();
