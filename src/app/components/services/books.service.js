(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('books', books);

  /** @ngInject */
  function books(Restangular) {
    var service = Restangular.all("books");
    service.addRestangularMethod('delete_book', 'post', 'delete_book');
    service.addRestangularMethod('status_book', 'post', 'status_book');
    service.addRestangularMethod('comment_book', 'post', 'comment_book');
    service.addRestangularMethod('update_price', 'post', 'update_price');
    return service;
  }

})();
