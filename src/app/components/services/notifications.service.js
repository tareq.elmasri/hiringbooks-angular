(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('Notifications', Notifications);

  /** @ngInject */
  function Notifications(Restangular) {
    var service = Restangular.all("notifications");
    service.addRestangularMethod('set_seen', 'post', 'set_seen');
    return service;
  }

})();
