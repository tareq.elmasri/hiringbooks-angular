(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('Search', Search);

  /** @ngInject */
  function Search(Restangular) {
    var service = Restangular.all("search");
    return service;
  }

})();
