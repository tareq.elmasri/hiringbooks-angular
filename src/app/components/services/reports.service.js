(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('Reports', Reports);

  /** @ngInject */
  function Reports(Restangular) {
    var service = Restangular.all("reports");
    service.addRestangularMethod('set_seen', 'post', 'set_seen');
    return service;
  }

})();
