(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('sidebar', sidebar);

  /** @ngInject */
  function sidebar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/sidebar/sidebar.html',
      scope: {
        addchatwindow: '='
      },
      controller: SidebarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function SidebarController($rootScope, $scope, $state, users, Notification, Reports) {
      var scope = this;
      scope.global = $rootScope;
      scope.newReport = "";
      scope.reportVisible = false;
      scope.navigations = [{
        name: "user",
        url: "activity/",
        title: "Acitivty"
      }, {
        name: "books",
        url: "books/",
        title: "Books"
      }, {
        name: "requests",
        url: "requests/",
        title: "Requests"
      }];
      $rootScope.$on("user_updated", function(data) {
        console.log(data);
        console.log($rootScope.selectedUser, $rootScope.user);
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      });

      scope.submitReport = function(user) {
        if (scope.newReport.length > 0) {
          user.reports_count++;
          Reports.post({
            "user_id": $rootScope.user.id,
            "target_user_id": user.id,
            "report": scope.newReport,
            "reports_count": user.reports_count,
            "timestamp": moment()
          }).then(function(data) {
            if (data.status) {
              scope.newReport = "";
              scope.reportVisible = false;
              Notification.success({
                message: "Your report has been submited successfully."
              });
            } else {
              Notification.failure({
                message: "Something wrong has happened."
              });
            }
          });
        }
      };

      scope.enterSubmit = function($event, user) {
        if ($event.keyCode == 13) {
          scope.submitReport(user);
        }
      };

      scope.rate = function(rate, isOff) {
        $rootScope.selectedUser.rating = parseFloat($rootScope.selectedUser.rating);
        rate = isOff ? $rootScope.selectedUser.rating + rate + 1 : rate + 1;
        console.log(typeof $rootScope.selectedUser.rating);
        users.rate_user({
          "user_id": $rootScope.user.id,
          "target_user_id": $rootScope.selectedUser.id,
          "ratings_count": $rootScope.selectedUser.ratings_count,
          "rating": rate
        }).then(function(data) {
          $rootScope.selectedUser = data;
        });
        if ($rootScope.selectedUser.rating === 0) {
          $rootScope.selectedUser.rating = rate;
        } else {
          $rootScope.selectedUser.rating = ($rootScope.selectedUser.rating + rate) / 2;
        }
        console.log($rootScope.selectedUser.rating);
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      };
      scope.activated_link = $state.current.name;
      scope.activate_link = function(name) {
        scope.activated_link = name;
      };
    }
  }

})();