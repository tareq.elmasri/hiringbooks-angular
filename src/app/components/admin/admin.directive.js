(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('admin', admin);

  /** @ngInject */
  function admin() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/admin/admin.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: AdminController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function AdminController($scope, $rootScope, $state, $location, $http, users, Reports, Notification) {
      var scope = this;
      scope.global = $rootScope;
      scope.selectedTab = 0;
      $rootScope.unseen_reports = 0;

      Reports.getList().then(function(data){
        $rootScope.reports = data;
        _.each(data, function(report) {
          if (report.seen === "0") {
            $rootScope.unseen_reports++;
          }
        });
        scope.setSeen();
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      });

      scope.deleteUser = function(user, $index) {
        users.delete_user({
          id: user.id
        }).then(function(data) {
          if (data.status !== false) {
            $rootScope.users.splice($index,1);
            Notification.success({
              message: "User deleted successfully."
            });
          }
        });
      };

      scope.suspendUser = function(user) {
        users.suspend_user({
          id: user.id,
          suspended: user.suspended === 0 ? 1 : 0
        }).then(function(data) {
          user.suspended = user.suspended === 0 ? 1 : 0;
          Notification.success({
            message: "User " + user.suspended === 0 ? "unsuspended" : "suspended" + " successfully."
          });
        });
      };

      scope.setSeen = function() {
        Reports.set_seen().then(function(data) {
          if (status !== false) {
            $rootScope.unseen_reports = 0;
          }
        });
      };

      scope.findUser = function(user_id) {
        var user = _.find($rootScope.users, function(user) {
          return user.id == user_id;
        });
        return user;
      };

      scope.navigateToUser = function(user) {
        $state.go('user', {
          user_id: user.id
        });
      };
    }
  }
})();