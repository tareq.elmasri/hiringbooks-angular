(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('navbar', navbar);

  /** @ngInject */
  function navbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
        creationDate: '=',
        addchatwindow: '=',
        guest: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($rootScope, $state, Notifications) {
      var scope = this;
      scope.global = $rootScope;
      scope.profileSettings = false;
      scope.messagesVisible = false;
      scope.notificationsVisible = false;
      scope.finduser = function(entity) {
        return _.findIndex($rootScope.users, function(user) {
          return user.id == entity.user_id;
        });
      };

      scope.setNotificationsSeen = function() {
        if (scope.notificationsVisible) {
          _.each(scope.global.user.notifications, function(notification, index) {
            if (notification.seen === 0) {
              Notifications.set_seen({
                id: notification.id
              }, function(data) {
                if (data.status !== false) {
                  scope.global.user.notifications_unseen = 0;
                  scope.global.user.notifications[index].seen = true;
                }
              });
            }
          });
        }
      };

      scope.openMessage = function(message, $index) {
        message.seen = 1;
        scope.global.user.messages_unseen.splice($index, 1);
        scope.addchatwindow(scope.global.users[scope.finduser(message)]);
        scope.messagesVisible = false;
      };

      scope.goToNotification = function(notification) {
        $state.go(notification.topic_type, {
          'user_id': $rootScope.user.id,
          'book_id': notification.topic_id
        });
      };
    }
  }

})();
