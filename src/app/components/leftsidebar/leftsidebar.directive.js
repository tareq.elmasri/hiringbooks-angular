(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('leftSidebar', leftSidebar);

  /** @ngInject */
  function leftSidebar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/leftsidebar/leftsidebar.html',
      scope: {
          user: '@',
          addchatwindow: '='
      },
      controller: LeftSidebarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function LeftSidebarController($scope, $rootScope) {
      var scope = this;
      scope.global = $rootScope;
      $rootScope.$on("users_updated", function(data){
        console.log(data);
        scope.global.users = $rootScope.users;
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      });
      scope.user_search = "";
    }
  }

})();
