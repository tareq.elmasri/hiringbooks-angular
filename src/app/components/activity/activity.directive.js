(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('activity', activity);

  /** @ngInject */
  function activity() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/activity/activity.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: ActivityController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function ActivityController($scope, $rootScope, $state, $location, $http, Wall) {
      var scope = this;
      scope.global = $rootScope;
      scope.selectedTab = 0;
      scope.newWallPost = "";
      scope.editMode = false;
      scope.commentMode = false;
      scope.submitLoading = false;
      scope.unseen_posts = 0;
      scope.newComment = "";
      scope.searchedPost = {};

      scope.setSeen = function() {
        _.each($rootScope.user.wall, function(post){
          if (post.seen === 0) {
            scope.unseen_posts++;
          }
        });
        Wall.set_seen({id: $rootScope.user.id}, function(data) {
          if (status !== false) {
            scope.unseen_posts = 0;
          }
        });
      };

      scope.openPost = function() {
        if ($location.search().post_id > 0) {
          var postIndex = _.findIndex($rootScope.selectedUser.wall, function(post) {
            return post.id == $location.search().post_id;
          });
          if (postIndex >= 0) {
            scope.commentMode = true;
            scope.global.selectedUser.posts[postIndex].commentMode = true;
            scope.selectedBook = scope.global.selectedUser.posts[postIndex];
          }
        }
      };

      $rootScope.$watchCollection('selectedUser', scope.openPost, true);
      $rootScope.$on('$locationChangeSuccess', scope.openPost);

      scope.stateReplace = function(text) {
        return text.replace("{state}", $rootScope.selectedUserIsMe ? "your" : $rootScope.selectedUser.fullname.split(" ")[0] + "'s");
      };

      scope.findUser = function(user_id) {
        var user = _.find($rootScope.users, function(user) {
          return user.id == user_id;
        });
        return user;
      };

      scope.navigateToActivity = function(activity) {
        $state.go(activity.topic_type, {
          user_id: activity.user_id,
          post_id: activity.topic_id
        });
      };

      scope.navigateToUser = function(user) {
        $state.go('user', {
          user_id: user.id
        });
      };

      scope.getActivity = function(activity) {
        var target_activity = _.find($rootScope.selectedUser[activity.topic_type], function(this_activity) {
          return activity.topic_id == this_activity.id;
        });
        return target_activity;
      };

      scope.writeWallPost = function($event) {
        if ($event.keyCode == 13) {
          $event.preventDefault();
          if ($event.target.value.length > 0) {
            var activity = $rootScope.selectedUserIsMe ? $rootScope.gender : "{state}";
            Wall.post({
              comment: $event.target.value,
              timestamp: moment(),
              user_id: $rootScope.user.id,
              seen: false,
              activity: "posted on " + activity + " wall.",
              target_user_id: $rootScope.selectedUser.id,
            }).then(function(data) {
              $rootScope.selectedUser.wall.unshift(data);
              $event.target.value = "";
            });
          }
        }
      };

      scope.comment = function($event, post) {
        if ($event.keyCode == 13) {
          $event.preventDefault();
          if ($event.target.value.length > 0) {
            post.comments_count++;
            Wall.comment_post({
              comment: $event.target.value,
              timestamp: moment(),
              topic_id: post.id,
              topic_type: 'wall',
              user_id: $rootScope.user.id,
              comments_count: post.comments_count,
              fullname: $rootScope.user.fullname,
              target_user_id: $rootScope.selectedUser.id,
            }).then(function(data) {
              $rootScope.selectedUser.wall_comments.unshift(data);
              $event.target.value = "";
            });
          }
        }
      };

      scope.delete = function(post) {
        Wall.delete_post({
          id: $rootScope.user.wall[post].id
        }).then(function(data) {
          $rootScope.user.wall.splice(post, 1);
        });
      };

      scope.blur = function() {
        scope.commentMode = false;
        scope.selectedPost.commentMode = false;
        if (history.pushState) {
          $location.search("");
        }
      };

      scope.focus = function(post) {
        scope.commentMode = true;
        scope.selectedPost = post;
        scope.selectedPost.commentMode = true;
        if (history.pushState) {
          $location.search('post_id', post.id);
        }
      };
    }
  }
})();