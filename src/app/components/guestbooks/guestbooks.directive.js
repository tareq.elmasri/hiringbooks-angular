(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('guestBooks', guestBooks);

  /** @ngInject */
  function guestBooks() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/guestbooks/guestbooks.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: GuestBooksController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function GuestBooksController($scope, $rootScope, $filter, $state, $location, $http, books, Goodreads) {
      var scope = this;
      scope.$apply = $scope.$apply;
      scope.global = $rootScope;
      scope.isLoading = false;
      scope.selectedBook = {};
      scope.booksLoaded = false;
      scope.slickConfig = {
        dots: false,
        autoplay: true,
        initialSlide: 3,
        infinite: true,
        autoplaySpeed: 3000,
        centerMode: true,
        variableWidth: true,
        method: {}
      };

      scope.getPrimColor = function(color) {
        if (color != "#F0EFE1" &&$filter('rgbToHsl')(color).join(",") !== "NaN,NaN,NaN") {
          console.log($filter('rgbToHsl')(color).join(","));
          return $filter('rgbToHsl')(color).join(",");
        }
      };

      books.getList().then(function(data) {
        _.each(data, function(book) {
          book.color = "#F0EFE1";
          book.getPrimColor = scope.getPrimColor;
        });
        $rootScope.books = data;
        scope.booksLoaded = true;
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
        console.log($rootScope.books);
      });

      scope.openBook = function() {
        if ($location.search().book_id > 0) {
          var bookIndex = _.findIndex($rootScope.selectedUser.books, function(book) {
            return book.id == $location.search().book_id;
          });
          if (bookIndex >= 0) {
            scope.commentMode = true;
            scope.global.selectedUser.books[bookIndex].commentMode = true;
            scope.selectedBook = scope.global.selectedUser.books[bookIndex];
          }
        }
      };
      //
      // scope.searchBook = function($event) {
      //   if ($event.target.value.length > 0) {
      //     scope.isLoading = true;
      //     Goodreads.get($event.target.value, function(data) {
      //       scope.isLoading = false;
      //       scope.searchedBooks = data;
      //       Goodreads.set(scope.searchedBooks);
      //     });
      //   }
      // };

      scope.blur = function() {
        scope.commentMode = false;
        scope.selectedBook.commentMode = false;
        if (history.pushState) {
          $location.search("");
        }
      };

      scope.focus = function(book) {
        scope.commentMode = true;
        scope.selectedBook = book;
        scope.selectedBook.commentMode = true;
        if (history.pushState) {
          $location.search('book_id', book.id);
        }
      };

    }
  }

})();