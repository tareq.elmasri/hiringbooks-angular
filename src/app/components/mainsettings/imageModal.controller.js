(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .controller('ImageModalController', ImageModalController);

    /** @ngInject */
    function ImageModalController($scope, $uibModalInstance, picture) {
      $scope.picture = picture;
      $scope.croppedPicture = "";

      $scope.ok = function () {
        $uibModalInstance.close($scope.croppedPicture);
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }

})();
