(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('mainBooks', mainBooks);

  /** @ngInject */
  function mainBooks() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/mainbooks/mainbooks.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: MainBooksController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function MainBooksController($scope, $rootScope, $uibModal, $state, $location, $http, books, Goodreads) {
      var scope = this;
      scope.global = $rootScope;
      scope.search = "";
      scope.editMode = false;
      scope.isLoading = false;
      scope.commentMode = false;
      scope.searchedBooks = [];
      scope.newComment = "";
      scope.resultIsArray = true;
      scope.selectedBook = {};

      scope.openBook = function() {
        if ($location.search().book_id > 0) {
          var bookIndex = _.findIndex($rootScope.selectedUser.books, function(book) {
            return book.id == $location.search().book_id;
          });
          if (bookIndex >= 0) {
            scope.commentMode = true;
            scope.global.selectedUser.books[bookIndex].commentMode = true;
            scope.selectedBook = scope.global.selectedUser.books[bookIndex];
          }
        }
      };

      $rootScope.$watchCollection('selectedUser', scope.openBook, true);
      $rootScope.$on('$locationChangeSuccess', scope.openBook);

      scope.searchBook = function($event) {
        if ($event.target.value.length > 0) {
          scope.isLoading = true;
          Goodreads.get($event.target.value, function(data) {
            scope.isLoading = false;
            scope.searchedBooks = data;
            Goodreads.set(scope.searchedBooks);
          });
        }
      };

      scope.submit = function(book) {
        book.submitLoading = true;
        book.user_id = $rootScope.user.id;
        book.fullname = $rootScope.user.fullname;
        book.timestamp = moment();
        var modalInstance = $uibModal.open({
          size: 'sm',
          animation: true,
          templateUrl: 'app/components/mainbooks/bookModal.html',
          controller: 'BookModalController',
          resolve: {
            price: function() {
              return 0;
            }
          }
        });

        modalInstance.result.then(function(price) {
          book.price = price;
          book.fullname = $rootScope.user.fullname;
          books.post(book).then(function(data) {
            console.log(data);
            book.mine = true;
            book.submitLoading = false;
            $rootScope.user.books.unshift(data);
          });
        });
      };

      scope.comment = function($event, book) {
        if ($event.keyCode == 13) {
          $event.preventDefault();
          if ($event.target.value.length > 0) {
            book.comments_count++;
            books.comment_book({
              comment: $event.target.value,
              timestamp: moment(),
              topic_id: book.id,
              topic_type: 'book',
              user_id: $rootScope.user.id,
              comments_count: book.comments_count,
              fullname: $rootScope.user.fullname,
              target_user_id: $rootScope.selectedUser.id,
            }).then(function(data) {
              $rootScope.selectedUser.books_comments.unshift(data);
              $event.target.value = "";
            });
          }
        }
      };

      scope.findcommentuser = function(comment) {
        return _.findIndex($rootScope.users, function(user) {
          return user.id == comment.user_id;
        });
      };

      scope.status = function(book) {
        var new_status = book.status === 0 ? 1 : 0;
        books.status_book({
          id: book.id,
          status: new_status
        }).then(function(data) {
          book.status = new_status;
        });
      };

      scope.delete = function(book) {
        books.delete_book({
          id: $rootScope.user.books[book].id
        }).then(function(data) {
          $rootScope.user.books.splice(book, 1);
        });
      };

      scope.blur = function() {
        scope.commentMode = false;
        scope.selectedBook.commentMode = false;
        if (history.pushState) {
          $location.search("");
        }
      };

      scope.focus = function(book) {
        scope.commentMode = true;
        scope.selectedBook = book;
        scope.selectedBook.commentMode = true;
        if (history.pushState) {
          $location.search('book_id', book.id);
        }
      };

      scope.changePrice = function(book) {
        var modalInstance = $uibModal.open({
          size: 'sm',
          animation: true,
          templateUrl: 'app/components/mainbooks/bookModal.html',
          controller: 'BookModalController',
          resolve: {
            price: function() {
              return book.price;
            }
          }
        });

        modalInstance.result.then(function(price) {
          books.update_price({
            id: book.id,
            price: price
          }).then(function(data) {
            book.price = price;
          });
        });
      };
    }
  }

})();