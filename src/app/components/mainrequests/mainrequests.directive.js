(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('mainRequests', mainRequests);

  /** @ngInject */
  function mainRequests() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/mainrequests/mainrequests.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: MainRequestsController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function MainRequestsController($scope, $rootScope, $state, $location, $http, Requests, Goodreads) {
      var scope = this;
      scope.global = $rootScope;
      scope.search = "";
      scope.editMode = false;
      scope.isLoading = false;
      scope.commentMode = false;
      scope.searchedBooks = [];
      scope.newComment = "";
      scope.resultIsArray = true;
      scope.selectedRequest = {};

      scope.openRequest = function() {
        if ($location.search().book_id > 0) {
          var requestIndex = _.findIndex($rootScope.selectedUser.requests, function(request) {
            return request.id == $location.search().book_id;
          });
          if (requestIndex >= 0) {
            scope.commentMode = true;
            scope.global.selectedUser.requests[requestIndex].commentMode = true;
            scope.selectedRequest = scope.global.selectedUser.requests[requestIndex];
          }
        }
      };

      $rootScope.$watchCollection('selectedUser', scope.openRequest, true);
      $rootScope.$on('$locationChangeSuccess', scope.openRequest);

      scope.searchBook = function($event) {
        if ($event.target.value.length > 0) {
          scope.isLoading = true;
          Goodreads.get($event.target.value, function(data) {
            scope.isLoading = false;
            scope.searchedBooks = data;
            Goodreads.set(scope.searchedBooks);
          });
        }
      };

      scope.submit = function(request) {
        request.submitLoading = true;
        request.user_id = $rootScope.user.id;
        request.fullname = $rootScope.user.fullname;
        request.timestamp = moment();
        Requests.post(request).then(function(data) {
          console.log(data);
          request.mine = true;
          request.submitLoading = false;
          $rootScope.user.requests.unshift(data);
        });
      };

      scope.comment = function($event, request) {
        if ($event.keyCode == 13) {
          $event.preventDefault();
          if ($event.target.value.length > 0) {
            request.comments_count++;
            Requests.comment_request({
              comment: $event.target.value,
              timestamp: moment(),
              topic_id: request.id,
              topic_type: 'request',
              user_id: $rootScope.user.id,
              comments_count: request.comments_count,
              fullname: $rootScope.user.fullname,
              target_user_id: $rootScope.selectedUser.id,
            }).then(function(data) {
              $rootScope.selectedUser.requests_comments.unshift(data);
              $event.target.value = "";
            });
          }
        }
      };

      scope.findcommentuser = function(comment) {
        return _.findIndex($rootScope.users, function(user) {
          return user.id == comment.user_id;
        });
      };

      scope.status = function(request) {
        var new_status = request.status === 0 ? 1 : 0;
        Requests.status_request({
          id: request.id,
          status: new_status
        }).then(function(data) {
          request.status = new_status;
        });
      };

      scope.delete = function(request) {
        Requests.delete_request({
          id: $rootScope.user.requests[request].id
        }).then(function(data) {
          $rootScope.user.requests.splice(request, 1);
        });
      };

      scope.blur = function() {
        scope.commentMode = false;
        scope.selectedRequest.commentMode = false;
        if (history.pushState) {
          $location.search("");
        }
      };

      scope.focus = function(request) {
        scope.commentMode = true;
        scope.selectedRequest = request;
        scope.selectedRequest.commentMode = true;
        if (history.pushState) {
          $location.search('book_id', request.id);
        }
      };
    }
  }

})();