/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('autoFocus', function() {
      return {
        link: {
          pre: function preLink(scope, element, attr) {
            // this fails since the element hasn't rendered
            //element[0].focus();
          },
          post: function postLink(scope, element, attr) {
            // this succeeds since the element has been rendered
            element[0].focus();
          }
        }
      };
    })
    .directive('checkForPagination', function() {
      return {
        link: function(scope, element, attr) {
          element.scroll(function() {
            if (element.scrollTop() <= 0) {
              var fetchMesesages = scope.$eval(attr.checkForPagination);
              fetchMesesages();
              element.css({
                scrollTop: 5
              });
            }
          });
        }
      };
    })
    .directive('fileButton', function() {
      return {
        link: {
          pre: function preLink(scope, element, attr) {
            // this fails since the element hasn't rendered
            //element[0].focus();
          },
          post: function postLink(scope, element, attr) {
            element.bind('click', function() {
              document.getElementById('fileInput').click();
            });
          }
        }
      };
    });
})();