/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    // .constant('socketpath', "hidden-cliffs-17245.herokuapp.com")
    .constant('backendpath', "localhost")
    .constant('socketpath', "localhost:7000")
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    ;
})();
