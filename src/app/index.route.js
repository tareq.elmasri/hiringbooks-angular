(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('user', {
        url: '/users/:user_id?post_id',
        params: {'user_id':""},
        reloadOnSearch: false,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('admin', {
        url: '/admin/:user_id',
        params: {'user_id':""},
        reloadOnSearch: false,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('books', {
        url: '/books/:user_id?book_id',
        params: {'user_id':""},
        reloadOnSearch: false,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('requests', {
        url: '/requests/:user_id?book_id',
        params: {'user_id':""},
        reloadOnSearch: false,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('settings', {
        url: '/settings',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('search', {
        url: '/search?keyword',
        reloadOnSearch: false,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })

      .state('guestsearch', {
        url: '/guestsearch?keyword',
        reloadOnSearch: false,
        templateUrl: 'app/guest/guest.html',
        controller: 'GuestController',
        controllerAs: 'vm'
      })

      .state('guestbooks', {
        url: '/guestbooks?bookid',
        reloadOnSearch: false,
        templateUrl: 'app/guest/guest.html',
        controller: 'GuestController',
        controllerAs: 'vm'
      })
    ;

    $urlRouterProvider.otherwise('/');
  }

})();
