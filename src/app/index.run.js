(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $window) {
    $rootScope.chat_windows = [];
    if (window.Notification.permission !== 0) {
      window.Notification.requestPermission();
    }
    $window.onfocus = function() {
      $rootScope.focus = true;
    };
    $window.onblur = function() {
      $rootScope.focus = false;
    };
  }

})();