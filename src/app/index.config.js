(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .config(config);

  /** @ngInject */
  function config($logProvider, $locationProvider, RestangularProvider, NotificationProvider) {
    RestangularProvider.setBaseUrl('/api/v1/');
    RestangularProvider.setDefaultHeaders({
      'Access-Control-Allow-Origin': '*'
    });

    $locationProvider.html5Mode(true);
    $logProvider.debugEnabled(true);

    NotificationProvider.setOptions({
      delay: 10000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'left',
      positionY: 'bottom'
    });
  }

})();