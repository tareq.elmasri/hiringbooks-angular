(function() {
  'use strict';

  angular
    .module('hiringbooksAngular', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'restangular',
      'ui.router',
      'ui.bootstrap',
      'luegg.directives',
      'angular-ladda',
      'ngImgCrop',
      'naif.base64',
      'vkEmojiPicker',
      'ui.bootstrap.popover',
      'angular-loading-bar',
      'ui-notification',
      'angular-web-notification',
      'slickCarousel',
      'ngColorThief'
    ]
  );

})();
