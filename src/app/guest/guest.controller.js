(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .controller('GuestController', GuestController);

  /** @ngInject */
  function GuestController($rootScope, $scope, $state, $location, users, books, Requests, Messages, Notification, webNotification, socketpath) {
    var scope = this;
    scope.global = $rootScope;
    $rootScope.guest = true;
    $rootScope.state = $state.current.name;

  }
})();